from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.utils.log import get_task_logger
from .crontab import beat_schedule

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'control2.settings')

logger = get_task_logger(__name__)

app = Celery('control2', broker=os.getenv('LOCAL_CELERY_BROKER_URL'), backend='rpc://', include=['core.tasks'])

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
# app.config_from_object('django.conf:settings', namespace='CELERY')
app.conf.broker_transport_options = {
    'max_retries': 3,
    'interval_start': 0,
    'interval_step': 0.2,
    'interval_max': 0.2,
}
app.conf.beat_schedule = beat_schedule

app.logger = logger
    
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
