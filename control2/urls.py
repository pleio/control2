"""control2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

import core.views as core_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('download-backup/<int:task_id>', core_views.download_backup, name='download_backup'),
    path('', core_views.home, name='home'),
    path('agreements', core_views.agreements),
    path('agreements/<int:cluster_id>', core_views.agreements),
    path('agreements/add/<int:cluster_id>', core_views.agreements_add),
    path('agreements/add_version/<int:cluster_id>/<int:agreement_id>', core_views.agreements_add_version),
    path('site/<int:site_id>', core_views.site),
    path('sites', core_views.sites, name='sites'),
    path('sites/sync', core_views.sites_sync, name="sites_sync"),
    path('sites/add', core_views.sites_add, name="site_add"),
    path('sites/delete/<int:site_id>', core_views.sites_delete, name="site_delete"),
    path('sites/copy/<int:site_id>', core_views.sites_copy, name="site_copy"),
    path('sites/backup/<int:site_id>', core_views.sites_backup, name='site_backup'),
    path('sites/enable/<int:site_id>', core_views.sites_enable, name="site_enable"),
    path('sites/disable/<int:site_id>', core_views.sites_disable, name="site_disable"),
    path('sites/admin', core_views.sites_admin),
    path('sites/email', core_views.sites_user_email),
    path('tasks', core_views.tasks, name='tasks'),
    path('tools', core_views.tools, name='tools'),
]
