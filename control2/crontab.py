from celery.schedules import crontab

beat_schedule = {
    'poll_remote_task': {
        'task': 'core.tasks.poll_remote_task',
        'schedule': 10.0,
    },
    'daily': {
        'task': 'core.tasks.dispatch_crons',
        'schedule': crontab(minute=0, hour=2),
        'args': ['daily']
    }
}
