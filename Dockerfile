FROM python:3.8-slim AS build

RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential \
    libpq-dev \
    python3-dev

RUN python -m venv /app-tmp/venv && /app-tmp/venv/bin/pip install --upgrade pip

WORKDIR /app-tmp
COPY requirements.txt /app-tmp
RUN /app-tmp/venv/bin/pip3 install -r requirements.txt --use-feature=2020-resolver


FROM python:3.8-slim

# Workaround for error with postgresql-client package
RUN mkdir -p /usr/share/man/man1/ /usr/share/man/man3/ /usr/share/man/man7/

RUN apt-get update && apt-get install --no-install-recommends -y \
    libgnutls30 \
    mime-support \
    ca-certificates

COPY --from=build /app-tmp/venv /app-tmp/venv
ENV PATH="/app-tmp/venv/bin:${PATH}"

WORKDIR /app
# App assets
COPY . /app
COPY ./docker/*.sh /
COPY ./docker/*.ini /
RUN chmod +x /*.sh
RUN chmod +r /*.ini

# Create media and static folders
RUN mkdir -p /app/static && chown www-data:www-data /app/static

ENV PYTHONUNBUFFERED 1
EXPOSE 8000
USER www-data
