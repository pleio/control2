{{- define "control2.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "control2.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "control2.labels" -}}
helm.sh/chart: {{ include "control2.chart" . }}
{{ include "control2.selectorLabels" . }}
{{- end -}}

{{- define "control2.selectorLabels" -}}
app.kubernetes.io/name: {{ include "control2.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
