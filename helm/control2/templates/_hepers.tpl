{{- define "control2.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "control2.secretsName" -}}
{{- printf "%s-secrets" ((include "control2.name" .)) -}}
{{- end -}}

{{- define "control2.tlsSecretName" -}}
{{- printf "tls-%s" ((include "control2.name" .)) -}}
{{- end -}}

{{- define "control2.mediaStorageName" -}}
{{- if .Values.storage.existingMediaStorage -}}
{{- .Values.storage.existingMediaStorage -}}
{{- else -}}
{{- printf "%s-media" ((include "control2.name" . )) -}}
{{- end -}}
{{- end -}}

{{- define "control2.backupStorageName" -}}
{{- if .Values.storage.existingBackupStorage -}}
{{- .Values.storage.existingBackupStorage -}}
{{- else -}}
{{- printf "%s-backup" ((include "control2.name" . )) -}}
{{- end -}}
{{- end -}}

{{- define "control2.backgroundSchedulerName" -}}
{{- printf "%s-background-scheduler" ((include "control2.name" . )) -}}
{{- end -}}

{{- define "control2.backgroundWorkerName" -}}
{{- printf "%s-background-worker" ((include "control2.name" . )) -}}
{{- end -}}

{{- define "control2.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "control2.labels" -}}
helm.sh/chart: {{ include "control2.chart" . }}
{{ include "control2.selectorLabels" . }}
{{- end -}}

{{- define "control2.selectorLabels" -}}
app.kubernetes.io/name: {{ include "control2.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Component }}
app.kubernetes.io/component: {{ .Component }}
{{- end }}
{{- end -}}
