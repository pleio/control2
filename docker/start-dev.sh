#!/usr/bin/env bash

# Collect static
python /app/manage.py collectstatic --noinput

python /app/manage.py migrate

# Start Gunicorn processes
echo Starting uwsgi
uwsgi --ini /uwsgi-dev.ini
