#!/usr/bin/env bash

# Collect static
python /app/manage.py collectstatic --noinput

# Run migrations
python /app/manage.py migrate

# Start Gunicorn processes
echo Starting uwsgi
uwsgi --ini /uwsgi-prod.ini