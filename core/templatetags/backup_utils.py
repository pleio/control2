import os

from django import template
from django.conf import settings
from django.urls import reverse

from core.models import ClusterTask

register = template.Library()


@register.simple_tag
def backup_url(cluster_task: ClusterTask):
    if cluster_task.state == 'SUCCESS' and cluster_task.response:
        backup_file = os.path.join(settings.BACKUP_PATH, cluster_task.response)
        if os.path.exists(backup_file) and os.path.isfile(backup_file):
            return reverse('download_backup', args=(cluster_task.id,))


@register.simple_tag
def if_without_files(cluster_task: ClusterTask, if_true_message, else_message=''):
    if cluster_task.arguments[1]:
        return if_true_message
    return else_message
