import logging
import django_filters

from django.db import models
from django.forms import Select, TextInput
from django.utils import timezone
from django.utils.module_loading import import_string

from core.celery_cluster import CeleryCluster

logger = logging.getLogger(__name__)


class Cluster(models.Model):
    class Meta:
        ordering = ['name']

    name = models.CharField(max_length=255, unique=True)
    backend_version = models.PositiveSmallIntegerField(default=2)
    celery_broker = models.CharField(max_length=255, default='')
    celery_backend = models.CharField(max_length=255, default='')
    is_test = models.BooleanField(default=False)

    use_tls = models.BooleanField(default=False)

    tls_key_file = models.CharField(max_length=255, null=True, blank=True)
    tls_cert_file = models.CharField(max_length=255, null=True, blank=True)
    ca_cert_file = models.CharField(max_length=255, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def celery(self):
        instance = CeleryCluster.Instance(self.id)
        return instance.app

    def execute_remote_task(self, task, arguments, callback=None):
        # create celery app
        # execute remote task
        # execute callback with taskID polling for results with timeout and X retries ?
        pass

    @property
    def site_count(self):
        return self.sites.filter(is_active=True).count()

    def __str__(self):
        return self.name


class ClusterTaskManager(models.Manager):
    def create_task(self, cluster: Cluster, name, arguments=None, **kwargs):
        remote_task = cluster.celery.send_task(name, arguments)

        task = self.model(
            cluster=cluster,
            task_id=remote_task.id,
            state=remote_task.state,
            name=name,
            arguments=arguments,
            **kwargs,
        )

        task.save()

        return task


class ClusterTask(models.Model):
    STATE_TYPES = (
        ('PENDING', 'PENDING'),
        ('STARTED', 'STARTED'),
        ('RETRY', 'RETRY'),
        ('FAILURE', 'FAILURE'),
        ('SUCCESS', 'SUCCESS'),
    )

    objects = ClusterTaskManager()

    class Meta:
        ordering = ['-created_at']

    author = models.ForeignKey('auth.User', null=True, on_delete=models.CASCADE, related_name='tasks')
    site = models.ForeignKey('core.Site', null=True, on_delete=models.CASCADE, related_name='tasks')
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='tasks')

    task_id = models.CharField(max_length=255)
    name = models.CharField(max_length=255)

    origin = models.CharField(max_length=255, default='')
    followup = models.CharField(max_length=255, null=True)

    arguments = models.JSONField(null=True, blank=True)
    response = models.JSONField(null=True, blank=True)

    state = models.CharField(
        max_length=16,
        choices=STATE_TYPES,
        default='PENDING'
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def run_followup(self):
        try:
            if not self.followup:
                return

            followup = import_string(self.followup)
            followup.delay(self.id)
        except Exception:
            pass


class Site(models.Model):
    class Meta:
        ordering = ['domain']

    sync_id = models.PositiveIntegerField(null=False, default=0)
    schema = models.CharField(max_length=255)
    domain = models.CharField(max_length=255)

    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='sites')

    is_active = models.BooleanField(default=True)
    agreements_accepted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def is_test(self):
        return self.cluster.is_test

    def __str__(self):
        return self.domain

    @property
    def disk_size(self):
        return Stat.objects.filter(site=self, stat_type='DISK_SIZE').latest('created_at').value

    @property
    def db_size(self):
        return Stat.objects.filter(site=self, stat_type='DB_SIZE').latest('created_at').value


class SiteFilter(django_filters.FilterSet):
    # pylint: disable=keyword-arg-before-vararg

    def __init__(self, data=None, *args, **kwargs):
        # if filterset is bound, use initial values as defaults
        if data is not None:
            # get a mutable copy of the QueryDict
            data = data.copy()

            for name, f in self.base_filters.items():
                initial = f.extra.get('initial')

                # filter param is either missing or empty, use initial as default
                if not data.get(name) and initial:
                    data[name] = initial

        super().__init__(data, *args, **kwargs)

    domain = django_filters.CharFilter(
        lookup_expr='icontains',
        widget=TextInput(attrs={'class': 'form-control', 'placeholder': 'Domein'})
    )
    cluster = django_filters.ModelChoiceFilter(
        queryset=Cluster.objects.all(),
        widget=Select(attrs={'class': 'form-select'})
    )
    is_active = django_filters.BooleanFilter(
        widget=Select(attrs={'class': 'form-control'},
                      choices=[(True, 'Active sites'), (False, 'Disabled sites')]),
        initial=True
    )

    class Meta:
        model = Site
        fields = ['cluster', 'domain', 'is_active']


class TaskFilter(django_filters.FilterSet):
    cluster = django_filters.ModelChoiceFilter(
        queryset=Cluster.objects.all(),
        widget=Select(attrs={'class': 'form-select'})
    )

    class Meta:
        model = ClusterTask
        fields = ['cluster']


class Stat(models.Model):
    STAT_TYPES = (
        ('DISK_SIZE', 'DISK_SIZE'),
        ('DB_SIZE', 'DB_SIZE'),
    )

    stat_type = models.CharField(
        max_length=16,
        choices=STAT_TYPES,
        default='DISK_SIZE'
    )

    site = models.ForeignKey(Site, on_delete=models.CASCADE, related_name='stats')
    value = models.BigIntegerField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)


class Agreement(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField()

    def __str__(self):
        return self.name


class AgreementVersion(models.Model):
    class Meta:
        ordering = ['created_at']

    agreement = models.ForeignKey(
        'core.Agreement',
        on_delete=models.CASCADE,
        related_name='versions'
    )
    version = models.CharField(max_length=100)
    document = models.FileField(upload_to='agreements')
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.agreement.name} {self.version}"
