from __future__ import unicode_literals

from django.contrib import admin
from core.models import Cluster, Site, ClusterTask


class ClusterAdmin(admin.ModelAdmin):
    pass


class SiteAdmin(admin.ModelAdmin):
    pass


class ClusterTaskAdmin(admin.ModelAdmin):
    pass


admin.site.register(Cluster, ClusterAdmin)
admin.site.register(ClusterTask, ClusterTaskAdmin)
admin.site.register(Site, SiteAdmin)
