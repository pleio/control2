from __future__ import absolute_import, unicode_literals

from datetime import timedelta

from django.core.mail import send_mail
from django.template.loader import get_template, render_to_string
from django.utils import timezone

from control2.celery import app
from celery.utils.log import get_task_logger
from celery.result import allow_join_result, AsyncResult
from core.models import Cluster, ClusterTask, Site, Stat
from core.utils import get_base_url

logger = get_task_logger(__name__)


@app.task(bind=True)
def dispatch_crons(self, period):
    # pylint: disable=unused-argument
    '''
    Dispatch period cron tasks for all tenants
    '''
    for cluster in Cluster.objects.all():
        add_remote_task.delay(cluster.id, 'control.tasks.get_sites', None)

    for site in Site.objects.all():
        logger.info('Schedule cron %s for %s', period, site.schema)

        if period == 'daily':
            add_remote_task.delay(site.cluster.id, 'control.tasks.get_file_disk_usage', (site.schema,))
            add_remote_task.delay(site.cluster.id, 'control.tasks.get_db_disk_usage', (site.schema,))


@app.task(bind=True, ignore_result=True)
def poll_remote_task(self):
    # pylint: disable=unused-argument
    '''
    Poll remote task status
    '''
    tasks = ClusterTask.objects.exclude(state__in=["SUCCESS", "FAILURE"])

    for task in tasks:
        remote_task = AsyncResult(task.task_id, app=task.cluster.celery)
        task.state = remote_task.state

        if remote_task.successful():
            task.response = remote_task.result

            if task.name == 'control.tasks.get_sites':
                update_sites.delay((task.cluster.id, task.response))

            if task.name == 'control.tasks.get_file_disk_usage':
                site = Site.objects.get(cluster__id=task.cluster.id, schema=task.arguments[0])
                Stat.objects.create(
                    stat_type='DISK_SIZE',
                    value=task.response,
                    site=site
                )

            if task.name == 'control.tasks.get_db_disk_usage':
                site = Site.objects.get(cluster__id=task.cluster.id, schema=task.arguments[0])
                Stat.objects.create(
                    stat_type='DB_SIZE',
                    value=task.response,
                    site=site
                )

            if task.name in ['control.tasks.add_site', 'control.tasks.delete_site', 'control.tasks.restore_site', 'control.tasks.update_site']:
                add_remote_task.delay(task.cluster.id, 'control.tasks.get_sites', None)

        elif remote_task.failed():
            task.response = {
                "error": str(remote_task.result)
            }
        else:
            # timeout task after 1 day
            if task.created_at < (timezone.now() - timedelta(days=1)):
                task.state = "FAILURE"
                task.response = "TIMEOUT"

        if task.followup:
            task.run_followup()

        task.save()


@app.task(bind=True, ignore_result=True)
def add_remote_task(self, cluster_id, task, arguments=None):
    # pylint: disable=unused-argument
    '''
    Add task from background!
    '''

    cluster = Cluster.objects.get(id=cluster_id)

    ClusterTask.objects.create_task(cluster=cluster, name=task, arguments=arguments)


@app.task(bind=True)
def remote_get_sites_admin(self, cluster_id):
    # pylint: disable=unused-argument
    cluster = Cluster.objects.get(id=cluster_id)
    task = cluster.celery.send_task('control.tasks.get_sites_admin')
    admins = []
    with allow_join_result():
        admins = task.get(timeout=50)

    return (cluster_id, admins)


@app.task(bind=True, ignore_result=True)
def update_sites(self, data):
    # pylint: disable=unused-argument
    logger.info(data)
    cluster_id, sites = data

    sync_ids = []

    cluster = Cluster.objects.get(id=cluster_id)
    for item in sites:
        site, created = Site.objects.get_or_create(sync_id=item.get("id"), cluster=cluster, defaults={
            'domain': item.get("domain"),
            'schema': item.get("name")
        })
        if created:
            site.is_active = item.get("is_active")
            site.agreements_accepted = item.get("agreements_accepted")
            site.save()
            logger.info("Added %i %s", site.sync_id, site.schema)
        else:
            logger.info("Already exists %i %s", site.sync_id, site.schema)

            # update the following attributes if changed
            attrs = ["domain", "is_active", "agreements_accepted"]

            for attr in attrs:
                if getattr(site, attr) != item.get(attr):
                    logger.info("Update %s for %i %s", attr, site.sync_id, site.domain)
                    setattr(site, attr, item.get(attr))
                    site.save()

        sync_ids.append(item.get("id"))

    # delete where sync_id is not in sync_ids
    delete = Site.objects.filter(cluster=cluster).exclude(sync_id__in=sync_ids)
    for item in delete:
        item.delete()


@app.task
def followup_backup_complete(task_id):
    task = ClusterTask.objects.get(id=task_id)

    if not task.site or not task.author:
        return

    context = {
        'task': task,
        'download': str(task.response).endswith('.zip'),
        'base_url': get_base_url()
    }

    if task.state == 'SUCCESS':
        content = render_to_string("mail/backup_success.txt", context)
    else:
        content = render_to_string("mail/backup_failure.txt", context)

    logger.warning("SEND MAIL TO %s", task.author.email)

    send_mail("Site backup complete [Control2]",
              message=content,
              from_email="info@pleio.nl",
              recipient_list=[task.author.email],
              fail_silently=False)

    logger.warning("SENT MAIL TO %s", task.author.email)

    return task.author.email
