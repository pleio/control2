import csv
import logging
import mimetypes
import os
import tempfile
from io import StringIO

from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from wsgiref.util import FileWrapper
from django.utils.text import slugify
from django.utils.timezone import localtime
from django.views.decorators.http import require_http_methods
from django.http import StreamingHttpResponse, FileResponse, HttpResponseNotFound
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.conf import settings
from core.models import Cluster, Site, SiteFilter, ClusterTask, TaskFilter
from core.tasks import add_remote_task, remote_get_sites_admin
from core.forms import (
    AddSiteForm, DeleteSiteForm, SitesAdminForm, CopySiteForm, ConfirmSiteForm, SitesEmailForm,
    AgreementAddForm, AgreementAddVersionForm, ConfirmSiteBackupForm
)

logger = logging.getLogger(__name__)


@login_required
def home(request):
    # pylint: disable=unused-argument

    context = {
        'clusters': Cluster.objects.all()
    }

    return render(request, 'home.html', context)


@login_required
def site(request, site_id):
    # pylint: disable=unused-argument

    site = Site.objects.get(id=site_id)
    db_size_dates = list(site.stats.filter(stat_type='DB_SIZE').values_list('created_at', flat=True))[-50:]
    db_size_data = list(site.stats.filter(stat_type='DB_SIZE').values_list('value', flat=True))[-50:]
    db_stat_days = []
    for date in db_size_dates:
        db_stat_days.append(date.strftime("%d-%b-%Y"))

    disk_size_dates = list(site.stats.filter(stat_type='DISK_SIZE').values_list('created_at', flat=True))[-50:]
    disk_size_data = list(site.stats.filter(stat_type='DISK_SIZE').values_list('value', flat=True))[-50:]
    disk_stat_days = []
    for date in disk_size_dates:
        disk_stat_days.append(date.strftime("%d-%b-%Y"))

    context = {
        'site': site,
        'db_stat_days': db_stat_days,
        'db_stat_data': db_size_data,
        'disk_stat_days': disk_stat_days,
        'disk_stat_data': disk_size_data
    }

    return render(request, 'site.html', context)


@login_required
def sites(request):
    # pylint: disable=unused-argument

    f = SiteFilter(
        request.GET,
        queryset=Site.objects.all()
    )

    page = request.GET.get('page', 1)

    paginator = Paginator(f.qs, 50)
    try:
        sites = paginator.page(page)
    except PageNotAnInteger:
        sites = paginator.page(1)
    except EmptyPage:
        sites = paginator.page(paginator.num_pages)

    context = {
        'filter': f,
        'sites': sites,
    }

    return render(request, 'sites.html', context)


@login_required
def sites_add(request):
    # pylint: disable=unused-argument

    if request.POST:
        form = AddSiteForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            cluster = data.get("cluster")
            schema = data.get("schema")
            domain = data.get("domain")
            backup = data.get("backup")

            if backup:
                add_remote_task.delay(cluster.id, 'control.tasks.restore_site', (backup, schema, domain))
            else:
                add_remote_task.delay(cluster.id, 'control.tasks.add_site', (schema, domain))

            messages.info(request, f"Add site {domain} op cluster '{cluster.name}' gestart in achtergrond")

            return redirect(reverse('sites'))
    else:
        form = AddSiteForm(initial={})

    context = {
        'form': form
    }

    return render(request, 'sites_add.html', context)


@login_required
@require_http_methods(["POST", "GET"])
def sites_delete(request, site_id):
    # pylint: disable=unused-argument

    site = Site.objects.get(id=site_id)

    if request.POST:
        form = DeleteSiteForm(request.POST)
        if form.is_valid():
            add_remote_task.delay(site.cluster.id, 'control.tasks.delete_site', (site.sync_id,))

            messages.info(request, f"Removed site {site.domain} op cluster '{site.cluster.name}' gestart in achtergrond")

            return redirect(reverse('sites'))

    else:
        form = DeleteSiteForm(initial={
            'site_id': site.id
        })

    context = {
        'site': site,
        'form': form
    }

    return render(request, 'sites_delete.html', context)


@login_required
@require_http_methods(["POST"])
def sites_sync(request):
    # pylint: disable=unused-argument

    for cluster in Cluster.objects.filter():
        add_remote_task.delay(cluster.id, 'control.tasks.get_sites', None)

        messages.info(request, f"Sync voor cluster '{cluster.name}'' gestart in achtergrond")

    return redirect(reverse('sites'))


@login_required
def sites_admin(request):
    # pylint: disable=unused-argument
    # pylint: disable=unused-variable

    if request.POST:
        form = SitesAdminForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            cluster = data.get("cluster")

            task = remote_get_sites_admin.delay(cluster.id)

            try:
                cluster_id, admins = task.get(timeout=60)

                def stream():
                    buffer = StringIO()
                    writer = csv.writer(buffer, delimiter=';', quotechar='"')
                    writer.writerow(['name', 'email', 'site', 'cluster_name'])
                    yield buffer.getvalue()

                    for admin in admins:
                        buffer = StringIO()
                        writer = csv.writer(buffer, delimiter=';', quotechar='"')
                        writer.writerow([admin['name'], admin['email'], admin['client_domain'], cluster.name])
                        yield buffer.getvalue()

                response = StreamingHttpResponse(stream(), content_type="text/csv")
                response['Content-Disposition'] = 'attachment; filename="site_admins.csv"'

                return response

            except Exception as e:
                messages.error(request, f"Error {e} getting admins from cluster '{cluster.name}'")
                form = SitesAdminForm(initial={})
    else:
        form = SitesAdminForm(initial={})

    context = {
        'form': form
    }

    return render(request, 'sites_admin.html', context)


@login_required
@require_http_methods(["POST", "GET"])
def sites_copy(request, site_id):
    # pylint: disable=unused-argument

    site = Site.objects.get(id=site_id)

    if request.POST:
        form = CopySiteForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            target_schema = data.get("target_schema")
            target_domain = data.get("target_domain")

            add_remote_task.delay(site.cluster.id, 'core.tasks.remote_copy_site', (site.cluster.id, site.sync_id, target_schema, target_domain))

            messages.info(request, f"Copy site {site.domain} to '{target_domain}' started in background")

            return redirect(reverse('sites'))

    else:
        form = CopySiteForm(initial={
            'site_id': site.id
        })

    context = {
        'site': site,
        'form': form
    }

    return render(request, 'sites_copy.html', context)


@login_required
@require_http_methods(["POST", "GET"])
def sites_backup(request, site_id):
    # pylint: disable=unused-argument

    site = Site.objects.get(id=site_id)
    origin = "site_backup:%s" % site.id

    if request.POST:
        form = ConfirmSiteBackupForm(request.POST)
        if form.is_valid():
            ClusterTask.objects.create_task(site.cluster,
                                            name='control.tasks.backup_site',
                                            arguments=(site.sync_id,
                                                       not form.cleaned_data['include_files'],
                                                       _backup_foldername(site),
                                                       bool(form.cleaned_data['create_archive'])),
                                            origin=origin,
                                            author=request.user,
                                            followup='core.tasks.followup_backup_complete',
                                            site=site)
            messages.info(request, f"Backup site {site.domain} op cluster '{site.cluster.name}' gestart in achtergrond")
            return redirect(reverse('site_backup', args=(site_id,)))

    else:
        form = ConfirmSiteBackupForm(initial={
            'site_id': site.id
        })

    context = {
        'site': site,
        'form': form,
        'tasks': ClusterTask.objects.filter(origin=origin).order_by('-created_at')[:5]
    }

    return render(request, 'sites_backup.html', context)


@login_required
def download_backup(request, task_id):
    task = ClusterTask.objects.get(id=task_id)

    filepath = os.path.join(settings.BACKUP_PATH, task.response)
    if not os.path.isfile(filepath):
        return HttpResponseNotFound()

    logger.info("download_backup %s by %s",
                os.path.basename(filepath),
                request.user.email)

    chunk_size = 8192
    response = FileResponse(FileWrapper(open(filepath, 'rb'), chunk_size),
                            content_type=mimetypes.guess_type(filepath)[0])
    response['Content-Length'] = os.path.getsize(filepath)
    response['Content-Disposition'] = "attachment; filename=%s" % os.path.basename(filepath)
    return response


def _backup_foldername(site):
    return slugify("%s_%s" % (
        localtime().strftime("%Y%m%d%H%M%S"),
        site.schema,
    ))


@login_required
@require_http_methods(["POST", "GET"])
def sites_disable(request, site_id):
    # pylint: disable=unused-argument

    site = Site.objects.get(id=site_id)

    if request.POST:
        form = ConfirmSiteForm(request.POST)
        if form.is_valid():
            add_remote_task.delay(site.cluster.id, 'control.tasks.update_site', (site.sync_id, {"is_active": False}))

            messages.info(request, f"Disable site {site.domain} op cluster '{site.cluster.name}' gestart in achtergrond")

            return redirect(reverse('sites'))

    else:
        form = ConfirmSiteForm(initial={
            'site_id': site.id
        })

    context = {
        'site': site,
        'form': form
    }

    return render(request, 'sites_disable.html', context)


@require_http_methods(["POST", "GET"])
def sites_enable(request, site_id):
    # pylint: disable=unused-argument

    site = Site.objects.get(id=site_id)

    if request.POST:
        form = ConfirmSiteForm(request.POST)
        if form.is_valid():
            add_remote_task.delay(site.cluster.id, 'control.tasks.update_site', (site.sync_id, {"is_active": True}))

            messages.info(request, f"Backup site {site.domain} op cluster '{site.cluster.name}' gestart in achtergrond")

            return redirect(reverse('sites'))

    else:
        form = ConfirmSiteForm(initial={
            'site_id': site.id
        })

    context = {
        'site': site,
        'form': form
    }

    return render(request, 'sites_enable.html', context)


@login_required
def tasks(request):
    # pylint: disable=unused-argument
    page = request.GET.get('page', 1)

    f = TaskFilter(request.GET, queryset=ClusterTask.objects.all())

    paginator = Paginator(f.qs, 50)
    try:
        tasks = paginator.page(page)
    except PageNotAnInteger:
        tasks = paginator.page(1)
    except EmptyPage:
        tasks = paginator.page(paginator.num_pages)

    context = {
        'filter': f,
        'tasks': tasks,
    }

    return render(request, 'tasks.html', context)


@login_required
def sites_user_email(request):
    # pylint: disable=unused-argument
    # pylint: disable=unused-variable

    if request.POST:
        form = SitesEmailForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            cluster = data.get("cluster")
            email = data.get("email")

            try:
                task = cluster.celery.send_task('control.tasks.get_sites_by_email', (email,))
                sites = task.get(timeout=60)

                context = {
                    'form': form,
                    'sites': sites,
                }

                return render(request, 'sites_user_email.html', context)
            except Exception as e:
                messages.error(request, f"Error {e} getting sites from cluster '{cluster.name}'")
                form = SitesEmailForm(initial={})
    else:
        form = SitesEmailForm(initial={})

    context = {
        'form': form
    }

    return render(request, 'sites_user_email.html', context)


@login_required
def tools(request):
    # pylint: disable=unused-argument

    return render(request, 'tools.html')


@login_required
def agreements(request, cluster_id=None):
    # pylint: disable=unused-argument

    if not cluster_id:
        cluster = Cluster.objects.first()
        return redirect('/agreements/%i' % cluster.id)

    if cluster_id:
        cluster = Cluster.objects.get(id=cluster_id)

    agreements = None

    try:
        task = cluster.celery.send_task('control.tasks.get_agreements')
        agreements = task.get(timeout=60)

    except Exception as e:
        messages.error(request, f"Error {e} getting agreements from cluster '{cluster.name}'")

    context = {
        'clusters': Cluster.objects.all(),
        'selected': cluster,
        'agreements': agreements
    }

    return render(request, 'agreements.html', context)


@login_required
def agreements_add(request, cluster_id):
    # pylint: disable=unused-argument

    cluster = Cluster.objects.get(id=cluster_id)

    if request.POST:
        form = AgreementAddForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            name = data.get("name")

            add_remote_task.delay(cluster.id, 'control.tasks.add_agreement', (name,))

            messages.info(request, f"Add agreement {name} on cluster '{cluster.name}' started in background")

            return redirect('/agreements/%i' % cluster.id)
    else:
        form = AgreementAddForm(initial={
            'cluster_id': cluster_id,
        })

    context = {
        'form': form
    }

    return render(request, 'agreements_add.html', context)


@login_required
def agreements_add_version(request, cluster_id, agreement_id):
    # pylint: disable=unused-argument

    def handle_uploaded_file(f):
        folder = os.path.join(settings.BACKUP_PATH, '_control', 'agreements')
        try:
            os.makedirs(folder)
        except FileExistsError:
            pass
        _, temp_file_path = tempfile.mkstemp(dir=folder, suffix='.pdf')

        with open(temp_file_path, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

        return temp_file_path

    cluster = Cluster.objects.get(id=cluster_id)

    if request.POST:
        form = AgreementAddVersionForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data

            version = data.get("version")
            agreement_id = data.get("agreement_id")
            uploaded_file = handle_uploaded_file(request.FILES['document'])

            add_remote_task.delay(cluster.id, 'control.tasks.add_agreement_version', (agreement_id, version, uploaded_file))

            messages.info(request, f"Add version {version} on cluster '{cluster.name}' started in background")

            return redirect('/agreements/%i' % cluster.id)
    else:
        form = AgreementAddVersionForm(initial={
            'cluster_id': cluster_id,
            'agreement_id': agreement_id
        })

    context = {
        'form': form
    }

    return render(request, 'agreements_add.html', context)
