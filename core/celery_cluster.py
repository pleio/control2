from __future__ import absolute_import, unicode_literals
import os
import ssl
from celery import Celery
from celery.utils.log import get_task_logger
from django.apps import apps

logger = get_task_logger(__name__)

class Singleton:

    def __init__(self, cls):
        self._cls = cls
        self._instances = {}

    def Instance(self, cluster_id):
        try:
            return self._instances[cluster_id]
        except KeyError:
            self._instances[cluster_id] = self._cls(cluster_id)
            return self._instances[cluster_id]

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._cls)

@Singleton
class CeleryCluster:

    def __init__(self, cluster_id):
        Cluster = apps.get_model(app_label='core', model_name='Cluster')
        cluster = Cluster.objects.get(id=cluster_id)

        logger.info("Configure cluster %s with broker %s %s and backend %s %s", cluster.name, cluster.celery_broker,
                    os.getenv(cluster.celery_broker), cluster.celery_backend, os.getenv(cluster.celery_backend))

        self.app = Celery(cluster.name, set_as_current=False, broker=os.getenv(cluster.celery_broker),
                          backend=os.getenv(cluster.celery_backend), log=logger, include=[])

        if cluster.use_tls:

            if cluster.tls_key_file:
                self.app.conf.broker_use_ssl = {
                    'cert_reqs': ssl.CERT_REQUIRED,
                    'keyfile': cluster.tls_key_file,
                    'certfile': cluster.tls_cert_file,
                    'ca_certs': cluster.ca_cert_file,
                }
            else:
                self.app.conf.broker_use_ssl = {
                    'cert_reqs': ssl.CERT_NONE
                }

        self.app.conf.broker_transport_options = {
            'max_retries': 3,
            'interval_start': 0,
            'interval_step': 0.2,
            'interval_max': 0.2,
        }



    def __str__(self):
        return 'CeleryCluster object'
