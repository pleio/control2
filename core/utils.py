from django.conf import settings


def get_base_url():
    if settings.ENV == 'prod':
        return "https://%s" % settings.ALLOWED_HOSTS[0]
    return "http://localhost:8001"
