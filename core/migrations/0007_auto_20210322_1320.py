# Generated by Django 3.1.5 on 2021-03-22 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_clustertask'),
    ]

    operations = [
        migrations.AddField(
            model_name='cluster',
            name='celery_backend',
            field=models.CharField(default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='cluster',
            name='celery_broker',
            field=models.CharField(default='', max_length=255),
        ),
    ]
