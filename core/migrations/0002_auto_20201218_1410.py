# Generated by Django 3.1.4 on 2020-12-18 14:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='site',
            old_name='name',
            new_name='schema',
        ),
        migrations.RemoveField(
            model_name='site',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='site',
            name='is_test',
        ),
        migrations.AddField(
            model_name='site',
            name='sync_id',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
