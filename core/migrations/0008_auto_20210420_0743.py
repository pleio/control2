# Generated by Django 3.1.5 on 2021-04-20 07:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20210322_1320'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='clustertask',
            options={'ordering': ['-created_at']},
        ),
    ]
