# Generated by Django 3.2.5 on 2022-08-05 08:07

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_site_is_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='Agreement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('description', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='site',
            name='agreements_accepted',
            field=models.BooleanField(default=False),
        ),
        migrations.CreateModel(
            name='AgreementVersion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('version', models.CharField(max_length=100)),
                ('document', models.FileField(upload_to='agreements')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('agreement', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='versions', to='core.agreement')),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
    ]
